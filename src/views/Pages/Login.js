import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import axios from 'axios';
import {
  Button,
  TextField,
  Paper,
  ButtonBase
} from "@material-ui/core";
import {
  Card, 
  CardHeader, 
  CardBody,
  Container,
  Row,
  Col
} from 'reactstrap';


//START
const Login =()=>{

//INITIATORS
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");

//QUERIES
	const handleLogin = () => {
		axios.post('https://glacial-bayou-34265.herokuapp.com/login',{
			email,
			password
		}).then(res=>{
			sessionStorage.token = res.data.token;
			sessionStorage.user = JSON.stringify(res.data.user);
			window.location.replace('#/main');
		})
	}

//HANDLERS
	const handleEmailChange = e => {
		setEmail(e.target.value);
	}	
	const handlePasswordChange = e =>{
		setPassword(e.target.value);
	}

    const handleRouteToRegister=()=>{
        window.location.replace('#/register')
    }

//-------------JSX------------------
	return(
<React.Fragment >
<div id="loginBackground">
<Container >
    <Row>
    <Col md={{size:4, offset:4}} 
         xs="12" 
         sm="12">
      <Card id="card">
        <CardHeader id="cardHeader">
          Log in to Laguna Pools
        </CardHeader>
        <CardBody id="cardBody">
                <div id="loginTextField">
                <TextField
                id="textField1"
                label="Email"
                variant="outlined"
                color="secondary"
                fullWidth
                type={"email"}
                onChange={handleEmailChange}
                />
                </div>
                <div id="loginTextField">
                <TextField
                id="textField2"
                label="Password"
                variant="outlined"
                color="secondary"
                fullWidth
                type={"password"}
                onChange={handlePasswordChange}
                />
                </div>
                <div id="loginButton">
                <Button
                id="button"
                variant="contained"
                color="secondary"
                onClick={handleLogin}
                >Log in
                </Button>
                <ButtonBase 
                    id="btnRegister"
                    onClick={handleRouteToRegister}
                    ><small>Register here</small>
                </ButtonBase>
                </div>
        </CardBody>
      </Card>
      </Col>
    </Row>    
</Container>
</div>
</React.Fragment>      
		)
}

export default Login;