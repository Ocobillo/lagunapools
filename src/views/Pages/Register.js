import React, { useState, useEffect } from "react";
import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Grid,
  Box,
  Typography,
  ButtonBase
} from "@material-ui/core";
import axios from 'axios';

//MATERIAL UI STYLE


//REGISTER
const Register = () => {

	//INTIATORS
	const [open, setOpen] = useState(false);

	const [users, setUsers] = useState([]);
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [contactNumber, setContactNumber] = useState("");
	const [password, setPassword] = useState("");

	const [firstNameRequired, setFirstNameRequired] = useState(true);
	const [lastNameRequired, setLastNameRequired] = useState(true);
	const [emailRequired, setEmailRequired] = useState(true);
	const [contactNumberRequired, setContactNumberRequired] = useState(true);
	const [passwordRequired, setPasswordRequired] = useState(true);

	//QUERIES

	useEffect(()=>{
		axios.get('https://glacial-bayou-34265.herokuapp.com/showusers').then(res=>{
			setUsers(res.data)
		})
	}, [])

  const handleRegisteruser = ()=>{
    axios({
      method: "POST",
      url: "https://glacial-bayou-34265.herokuapp.com/register",
      data: {
        firstName:firstName,
        lastName:lastName,
        email:email,
        contactNumber:contactNumber,
        password:password
      }
    }).then(res=>{
      let newUser = [...users];
      newUser.push(res.data);
      setUsers(newUser);
    });
    handleRouteToLogin();
  }





  //HANDLERS
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleFirstNameRegister = (e) =>{
  	if(e.target.value===""){
  		setFirstNameRequired(true);
  		setFirstName("");
  	}else{
  		setFirstNameRequired(false);
  		setFirstName(e.target.value)
  	}
  }

  const handleLastNameRegister = (e) =>{
  	if(e.target.value===""){
  		setLastNameRequired(true);
  		setLastName("");
  	}else{
  		setLastNameRequired(false);
  		setLastName(e.target.value);
  	}
  }

  const handleEmailRegister = (e) =>{
    if(e.target.value===""){
      setEmailRequired(true);
      setEmail("");
    }else{
      setEmailRequired(false);
      setEmail(e.target.value);
    }
  }

  const handleContactNumberRegister = (e) =>{
    if(e.target.value===""){
      setContactNumberRequired(true);
      setContactNumber("");
    }else{
      setContactNumberRequired(false);
      setContactNumber(e.target.value);
    }
  }

  const handlePasswordRegister = (e) =>{
    if(e.target.value===""){
      setPasswordRequired(true);
      setPassword("");
    }else{
      setPasswordRequired(false);
      setPassword(e.target.value);
    }
  }

  const handleRouteToLogin=()=>{
    window.location.replace('#/login')
  }



  //------------ JSX -------------------
  return (
<React.Fragment>
      <Grid container id='registerbg'>
        <Grid item xs={12}>
          <Grid container justify="center" direction="column">
          <Box id="registerTitle">
                Laguna Pools
          </Box>
            <Dialog
              open={open}
              onClose={handleClose}
              aria-labelledby="form-dialog-title"
            >
              <DialogTitle id="registerNowForm">
              <h4 id="registerNowTitle">REGISTER NOW</h4>
              </DialogTitle>
              <DialogContent>
                <DialogContentText>
                  After this, you can start checking our resorts
                </DialogContentText>
                <TextField
                  autoFocus
                  margin="dense"
                  id="firstName"
                  label="First Name"
                  type="text"
                  required={firstNameRequired}
                  onChange={handleFirstNameRegister}
                  fullWidth
                />
                <TextField
                  autoFocus
                  margin="dense"
                  id="lastName"
                  label="Last Name"
                  type="text"
                  required={lastNameRequired}
                  onChange={handleLastNameRegister}
                  fullWidth
                />
                <TextField
                  autoFocus
                  margin="dense"
                  id="email"
                  label="Email Address"
                  type="email"
                  required={emailRequired}
                  onChange={handleEmailRegister}
                  fullWidth
                />
                <TextField
                  autoFocus
                  margin="dense"
                  id="contactNumber"
                  label="Mobile number"
                  type="number"
                  required={contactNumberRequired}
                  onChange={handleContactNumberRegister}
                  fullWidth
                />
                <TextField
                  autoFocus
                  margin="dense"
                  id="password"
                  label="Password"
                  type="password"
                  required={passwordRequired}
                  onChange={handlePasswordRegister}
                  fullWidth
                />
              </DialogContent>
              <DialogActions>
                <Button 
                  onClick={handleClose} 
                  variant='contained'
                  color="primary">

                  Cancel
                </Button>

                <Button 
                  onClick={handleRegisteruser} 
                  variant='contained'
                  color="primary">
                  Register
                </Button>
              </DialogActions>
            </Dialog>
              <Button
              variant="contained"
              id="registerbtn"
              color="primary"
              onClick={handleClickOpen}
                >Register
            </Button>
            <Button 
              color="secondary"
              id="loginbtn"
              onClick={handleRouteToLogin}
              >Log in
            </Button>
          </Grid>
        </Grid>
      </Grid>
</React.Fragment>      
  );
};

export default Register;
