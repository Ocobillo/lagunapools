import React, {useState} from 'react';
import {Button} from 'reactstrap';
import axios from 'axios';

const Images = props => {

    let imgUrls = props.imageUrls;

    return ( 
        <React.Fragment >
        <div 
        className = "col-lg-6 offset-lg-3">
        <input 
        className = "form-control"
        type = "file"
        multiple onChange = {props.selectImages}
        /> 
        <small>{props.infoMessage}</small> 
        </div> 
        
        </React.Fragment>
    )
}

export default Images;