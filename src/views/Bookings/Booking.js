import React, {useState, useEffect} from 'react';
import Stepper from './Stepper';
import 'date-fns';
import Grid from '@material-ui/core/Grid';
import DateFnsUtils from "@date-io/date-fns";
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker
} from '@material-ui/pickers';
import axios from 'axios';
import { format, differenceInCalendarDays } from 'date-fns';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
}));


const Booking = (props)=> {

const classes = useStyles();  
let post = props.post;

  //INITIATORS

  const [selectedDateFrom, setSelectedDateFrom] = useState(new Date());
  const [selectedDateTo, setSelectedDateTo] = useState(new Date());
  const [bookings, setBookings] = useState([]);
  const [showStepper, setShowStepper] = useState(false);
  const [dateChecker, setDatechecker] = useState(0);

  const [attendees , setAttendees] = useState("");
  const [attendeesRequired, setAttendeesRequired] = useState(true);

  const [transferDateFrom, setTransferDateFrom] = useState(new Date());
  const [transferDateTo, setTransferDateTo] = useState(new Date());

  const [formatFrom, setFormatFrom] = useState("");
  const [formatTo, setFormatTo] = useState("");
  const [totalFees, setTotalFees] = useState(0);

  //QUERIES
  useEffect(()=>{
   axios.get('https://glacial-bayou-34265.herokuapp.com/showbookings').then(res=>{
     setBookings(res.data);
   })
  }, [])



  const handleSaveDate = ()=>{

   axios({
     method:"POST",
     url:"https://glacial-bayou-34265.herokuapp.com/addbooking",
     data:{
       name:post.name,
       address:post.address,
       city:post.city,
       email:post.email,
       price: post.price,
       noOfAttendees: 20,
       dateFrom: formatFrom,
       dateTo: formatTo,
       differenceInDate: dateChecker,
       totalPayment: totalFees
     }
   }).then(res=>{
     let newBookings = [...bookings];
     newBookings.push(res.data);
     setBookings(newBookings);
   });
   console.log("success!")
  }
      

  //HANDLERS
  const handleDateChangeFrom = dateFrom => {
    setSelectedDateFrom(dateFrom);
  }

  const handleDateChangeTo = dateTo => {
    setSelectedDateTo(dateTo);
    
    let newFrom = new Date(selectedDateFrom);
    let formattedFrom = format(newFrom, 'yyyy, M, d');
    setFormatFrom(formattedFrom);

    let newTo = new Date(dateTo);
    let formattedTo = format(newTo, 'yyyy, M, d'); 
    setFormatTo(formattedTo);

    const result = differenceInCalendarDays(new Date(formattedTo), new Date(formattedFrom));
    setTotalFees(result*post.price);
    setDatechecker(result);
  }

  const handleAttendeesChanges = (e)=>{
    if(e.target.value==""){
      setAttendeesRequired(true);
      setAttendees("");
    }else{
      setAttendeesRequired(false)
      setAttendees(e.target.value);
    }
  }

  const handleShowStepper = (dateFrom, dateTo)=>{
    setShowStepper(!showStepper);
    setTransferDateFrom(selectedDateFrom);
    setTransferDateTo(selectedDateTo);
  }

 const handleDisableDays=(date)=> {
  DateFnsUtils.isToday(new Date)
}

//-----------JSX---------- 
  return (
<React.Fragment>
    <Stepper 
      showStepper={showStepper}
      handleShowStepper={handleShowStepper}
      transferDateFrom={transferDateFrom}
      transferDateTo={transferDateTo}
      post={post}
      dateChecker={dateChecker}
      handleSaveDate={handleSaveDate}
      attendees={attendees}
      sessionUser={props.sessionUser}
    />

    <MuiPickersUtilsProvider utils={DateFnsUtils}
      className={classes.root}
    >
      <Grid container 
        justify="flex-end"
      >

        <Grid item xs={12}>
          <TextField id="standard-basic"
          onChange={handleAttendeesChanges}
          required={attendeesRequired}
          label="Number of Attendees"
          />
        </Grid>

        <Grid item xs={6}>
        <KeyboardDatePicker
          disablePast
          margin="normal"
          id="date-picker-dialog"
          label="From"
          format="MM/dd/yyyy"
          value={selectedDateFrom}
          onChange={handleDateChangeFrom}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />
        </Grid>

        <Grid item xs={6}>
        <KeyboardDatePicker
          disablePast
          minDate={selectedDateFrom}
          className="my-3"
          margin="normal"
          id="date-picker-dialog"
          label="To"
          format="MM/dd/yyyy"
          value={selectedDateTo}
          onChange={handleDateChangeTo}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />
        </Grid>

        <Grid item>
        <Button variant="contained" color="primary"
        disabled={ dateChecker<=0 || post.capacity<attendees ? true: false}
        onClick={()=>handleShowStepper(selectedDateFrom, selectedDateTo)}
            >Send
        </Button>
        </Grid>




      </Grid>
    </MuiPickersUtilsProvider>
</React.Fragment>    
  );
}

export default Booking;