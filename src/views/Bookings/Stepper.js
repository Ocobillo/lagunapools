import React, {useState} from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {
	Modal, ModalHeader,
	ModalBody
 		} from 'reactstrap';	
import { format } from 'date-fns';
import Accordion from 'react-bootstrap/Accordion';
import {Row} from 'reactstrap';
import StripeCheckout from 'react-stripe-checkout';
import axios from 'axios';
import Swal from 'sweetalert2';

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));



//-----------------------------------------------
const SummaryOfBooking = (props)=>{


	//CATCHERS
	let dateFrom = props.transferDateFrom;
	let newDateFrom = format(dateFrom, 'm/d/yyyy');
	let dateTo = props.transferDateTo;
	let newDateTo = format(dateTo, 'm/d/yyyy');
	let post = props.post;
	let noOfDays = props.dateChecker;
	let price= post.price;
	let totalFee = noOfDays*price;
	let noOfAttendees = props.attendees;
	let user = props.sessionUser;


	//INTITIATORS
	const [bookings, setBookings] = useState([]);

	//QUERIES


	async function handleToken(token, addresses) {
	  const response = await axios.post(
	    "https://glacial-bayou-34265.herokuapp.com/checkout",
	    {token, post}
	  );
	  const { status } = response.data;
	  console.log("Response:", response.data);
	  if (status === "success") {

  		axios({
  	  		method: "POST",
  	  		url:"https://glacial-bayou-34265.herokuapp.com/addbooking",
  	  		data:{
  	  			name: post.name,
  	  			address: post.address,
  	  			price: post.price,
  	  			noOfAttendees: noOfAttendees,
  	  			dateFrom: Date(newDateFrom),
  	  			dateTo: Date(newDateTo),
  	  			noOfDays: noOfDays,
  	  			totalPayment: totalFee,
  	  			username: user.name,
  	  			emailOfUser: user.email,
  	  			isPaid: true
  	  		}
  	  	}).then(res=>{
  	     let newBookings = [...bookings];
  	     newBookings.push(res.data);
  	     setBookings(newBookings);
  	   });

	  	Swal.fire({
	  	  position: 'center',
	  	  icon: 'success',
	  	  title: 'Success! Payment has been processed',
	  	  showConfirmButton: false,
	  	  timer: 1500
	  	})
	  } else {
	  	Swal.fire({
	  	  position: 'center',
	  	  icon: 'error',
	  	  title: "Something went wrong",
	  	  showConfirmButton: false,
	  	  timer: 1500
	  	})
	  }

	}

//-------JSX-------	
	return(
	<React.Fragment>
	<Modal
		isOpen={props.showStepper}
		toggle={props.handleShowStepper}
	>
		<ModalHeader
		toggle={props.handleShowStepper}
		style={{backgroundColor:"#ff5722"}}
		>
		SUMMARY
		</ModalHeader>
		<ModalBody className="container">

			<div className="row">
				<div className="col md-12 lg-12">
					<h2>{post.name}</h2>
				</div>
			</div>

			<div className="row">
				<div className="col md-12 lg-12">
					<p>{post.address} {post.city}</p>
				</div>
			</div>

			<div className="row">
				<div className="col md-6 text-center">
					<h4>PRICE: <strong>{post.price}</strong></h4>
				</div>
				<div className="col md-6 text-center">
					<h4>TOTAL FEES: <strong>{totalFee}</strong></h4>
				</div>
			</div>

			<div className="row justify-content-center">
				<div 
					className="col md-3 text-center">
					<p>FROM: {newDateFrom}</p>
				</div>
				<div 
					className="col md-3 text-center">
					<p>TO: {newDateTo}</p>
				</div>
				<div 
					className="col md-4 text-center">
					<p>Total of <strong>{noOfDays} {noOfDays ==1 ? 'day':'days'}</strong></p>
				</div>
			</div>

			<div className="row justify-content-end">
				<div 
					className="col md-12 text-center">
					<StripeCheckout 
					stripeKey="pk_test_7VJYwxE5OIKcX4vnZs9XIYER00Lj5bmdSb"
					token={handleToken}
					billingaddress={post.address}
					amount={price*100}
					name={post.name}
					currency="PHP"
					/>
				</div>
			</div>
    	</ModalBody>
    </Modal>
	</React.Fragment>
		)
}
export default SummaryOfBooking;