import React from 'react';
import {
    Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle,
    Button, Container, Row, Col
} from 'reactstrap';


const CardPost = (props)=> {

    //INITIATORS
    //END OF INITIATORS


    //DECLARATIONS  
    const posts = props.posts;
    //END OF DECLARATIONS


    //HANDLES

    return ( 
        <React.Fragment>
<Container>
    <Row>
    {posts.map(post=>
        <Col xs="6" sm="4" key={post._id}>
        <Card className="my-3">
        <CardImg top width="100%" src="/assets/318x180.svg" alt="Card image cap" />
            <CardBody>

                <CardTitle>{post.name}</CardTitle>
                <CardSubtitle>{post.price}</CardSubtitle>
                <CardText>
                {post.summary}
                </CardText>
                <Button 
                    className="mx-1" 
                    outline color="secondary"
                    onClick={()=>props.handleShowLearnMore(post)}
                    > Reserve
                </Button>

                <Button 
                    className="mx-1" 
                    outline color="secondary"
                    onClick={()=>props.handleShowEditForm(post)}
                    >Edit
                </Button>

                <Button 
                    className="mx-1" 
                    outline color="secondary"
                    onClick={()=>props.handleDeletePost(post._id)}
                    >Delete
                </Button>

            </CardBody>
        </Card>
        </Col>
    )}            
    </Row>
</Container>    
        </React.Fragment>
    );
}
export default CardPost;