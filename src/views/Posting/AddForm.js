import React from 'react';
import {
	Modal, 
	ModalHeader,
	ModalBody, 
	Button,FormGroup      } from 'reactstrap';
import {FormInput} from '../Components';
import Image2 from '../Images/Image2';
import {Confirmation} from '../Components';
import {Checkbox, Grid, TextField} from '@material-ui/core';


const AddForm = (props)=>{

	const handleTesting=()=>{
		console.log("This is testing")
	}

//----JSX----
	return(
		<React.Fragment>
			<Modal
				isOpen={props.showForm}
				toggle={props.handleShowAddForm}
				onClosed={props.handleRefresh}
			>
			<ModalHeader
				toggle={props.handleShowAddForm}
				style={{backgroundColor:"#ff5722"}}
				>ADD NEW POST
			</ModalHeader>

			<ModalBody>
				<Grid item xs={12}>
				  <TextField id="standard-basic"
				  onChange={props.handlenameChange}
				  required={props.nameRequired}
				  label="Resort Name"
				  fullWidth
				  />
				</Grid>

				<Grid item xs={12}>
				  <TextField id="standard-basic"
				  onChange={props.handleAddressChange}
				  required={props.addressRequired}
				  label="Address"
				  fullWidth
				  />
				</Grid>

				<Grid item xs={12}>
				  <TextField id="standard-basic"
				  onChange={props.handlePriceChange}
				  required={props.priceRequired}
				  label="Price"
				  type="number"
				  fullWidth
				  />
				</Grid>

				<Grid item xs={12}>
				  <TextField id="standard-basic"
				  onChange={props.handleSummaryChange}
				  required={props.summaryRequired}
				  label="Summary"
				  fullWidth
				  />
				</Grid>

				<Grid item xs={12}>
				  <TextField id="standard-basic"
				  onChange={props.handleDescriptionChange}
				  required={props.descriptionRequired}
				  label="Description"
				  fullWidth
				  />
				</Grid>

				<Grid item xs={12}>
				  <TextField id="standard-basic"
				  onChange={props.handleNoOfRoomsChange}
				  required={props.noOfRoomsRequired}
				  label="Number of Rooms"
				  type='number'
				  fullWidth
				  />
				</Grid>

				<Grid item xs={12}>
				  <TextField id="standard-basic"
				  onChange={props.handleCapacityChange}
				  required={props.capacityRequired}
				  label="Capacity"
				  type='number'
				  fullWidth
				  />
				</Grid>

				<Grid item xs={12}>
				  <TextField id="standard-basic"
				  onChange={props.handleWebsiteChange}
				  required={props.websiteRequired}
				  label="Website"
				  fullWidth
				  />
				</Grid>

				<Grid item md={6}>
				<Checkbox
				        onChange={()=>props.handlePoolChange()}
				        value='props.isPoolChecked'
				        color="primary"
				        inputProps={{
				          'aria-label': 'secondary checkbox',
				        }}
				      /><label>Is there a Pool?</label>
				</Grid>

				<Grid item md={6}>
				<Checkbox
				        onChange={()=>props.handleKaraokeChange()}
				        value='props.checkedKaraoke'
				        color="primary"
				        inputProps={{
				          'aria-label': 'secondary checkbox',
				        }}
				      /><label>Is there a Karaoke?</label>
				</Grid>      

				<Grid item md={6}>     
				<Checkbox
				        onChange={()=>props.handleJacuzziChange()}
				        value='props.checkedJacuzzi'
				        color="primary"
				        inputProps={{
				          'aria-label': 'secondary checkbox',
				        }}
				      /><label>Is there a Jacuzzi?</label>
				</Grid>    
				  
				<Grid item md={6}>
				<Checkbox
				        onChange={()=>props.handleWifiChange()}
				        value='props.checkedWifi'
				        color="primary"
				        inputProps={{
				          'aria-label': 'secondary checkbox',
				        }}
				      /><label>Is there Wifi Connection?</label>
				</Grid>      

				<Grid item md={6}>      
				<Checkbox
				        onChange={()=>props.handleParkingChange()}
				        value='props.checkedParking'
				        color="primary"
				        inputProps={{
				          'aria-label': 'secondary checkbox',
				        }}
				      /><label>Is there a parking?</label>
				</Grid>      

				<Grid item md={6}>      
				<Checkbox
				        onChange={()=>props.handleRestaurantChange()}
				        value='props.checkedRestaurant'
				        color="primary"
				        inputProps={{
				          'aria-label': 'secondary checkbox',
				        }}
				      /><label>Is there a restaurant?</label>
				</Grid>      

				<Grid item md={6}>      
				<Checkbox
				        onChange={()=>props.handleIsPrivateChange()}
				        value='checkedIsPrivate'
				        color="primary"
				        inputProps={{
				          'aria-label': 'secondary checkbox',
				        }}
				      /><label>Is this a private resort?</label>
				</Grid>      

				<FormGroup>
					<Image2
					id="addForm-Image"	
					infoMessage={props.infoMessage}
					selectImages={props.selectImages}
					infoMessage={props.infoMessage}
					uploadImages={props.uploadImages}
					 />
				 </FormGroup>



				 <Confirmation 
				 	handleShowSaveConfirmation={props.handleShowSaveConfirmation}
				 	open={props.open}
				 	handleClose={props.handleClose}
				 	handleSavePost={props.handleSavePost}
				 />		  
			</ModalBody>
			</Modal>
		</React.Fragment>
		)
}
export default AddForm;