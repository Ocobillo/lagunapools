import React from 'react';
import {
	Modal, ModalHeader, ModalBody,
	Button, Container, Col, Row, 
						} from 'reactstrap';
import {  
	Grid, Box, Typography,
	Popover, makeStyles,
		                } from "@material-ui/core";		
import Booking from '../Bookings/Booking';
import Accordion from 'react-bootstrap/Accordion';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import KingBedIcon from '@material-ui/icons/KingBed';
import PersonIcon from '@material-ui/icons/Person';
import LocalOfferIcon from '@material-ui/icons/LocalOffer';
import BeachAccessIcon from '@material-ui/icons/BeachAccess';
import PoolIcon from '@material-ui/icons/Pool';                            
import MicIcon from '@material-ui/icons/Mic';
import HotTubIcon from '@material-ui/icons/HotTub';
import WifiIcon from '@material-ui/icons/Wifi';
import LocalParkingIcon from '@material-ui/icons/LocalParking';
import RestaurantIcon from '@material-ui/icons/Restaurant';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import {MouseOverPopover} from '../Components';
import Link from '@material-ui/core/Link';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: 2,
  },
  paper: {
    height: 140,
    width: 345,
    padding: theme.spacing(1),
  },
  control: {
    padding: theme.spacing(2),
  },
  card: {
    width: 345,
  },
  media: {
    height: 200,
  },
  popover: {
    pointerEvents: 'none',
  },
}));

const LearnMore = (props)=>{

	const classes = useStyles();

	//INITIATORS
	let post = props.postLearnMore;
	const [spacing, setSpacing] = React.useState(2);
	const [swimmingPool, setSwimmingPool] = React.useState(null);
	const popOpenPool = Boolean(swimmingPool);
	const [karaoke, setKaraoke] = React.useState(null);
	const popOpenKaraoke = Boolean(karaoke);

	const [jacuzzi, setJacuzzi] = React.useState(null);
	const popOpenJacuzzi = Boolean(jacuzzi);

	const [wifi, setWifi] = React.useState(null);
	const popOpenWifi = Boolean(wifi);

	const [parking, setParking] = React.useState(null);
	const popOpenParking = Boolean(parking);

	const [restaurant, setRestaurant] = React.useState(null);
	const popOpenRestaurant = Boolean(restaurant);

	const [isPrivate, setIsPrivate] = React.useState(null);
	const popOpenIsPrivate = Boolean(isPrivate);

    //HANDLERS
   //HANDLERS

     //SWIMMING POOL
     const handleChangePool = event => {
       setSpacing(Number(event.target.value));
     };
     const handlePopoverOpenPool = event => {
       setSwimmingPool(event.currentTarget);
     };
     const handlePopoverClosePool = () => {
       setSwimmingPool(null);
     };

     //KARAOKE
     const handleChangeKaraoke = event => {
       setSpacing(Number(event.target.value));
     };
     const handlePopoverOpenKaraoke = event => {
       setKaraoke(event.currentTarget);
     };
     const handlePopoverCloseKaraoke = () => {
       setKaraoke(null);
     };

     //JACUZZI
     const handleChangeJacuzzi = event => {
       setSpacing(Number(event.target.value));
     };
     const handlePopoverOpenJacuzzi = event => {
       setJacuzzi(event.currentTarget);
     };
     const handlePopoverCloseJacuzzi = () => {
       setJacuzzi(null);
     };

     //WIFI
     const handleChangeWifi = event => {
       setSpacing(Number(event.target.value));
     };
     const handlePopoverOpenWifi = event => {
       setWifi(event.currentTarget);
     };
     const handlePopoverCloseWifi = () => {
       setWifi(null);
     };

     //PARKING
     const handleChangeParking = event => {
       setSpacing(Number(event.target.value));
     };
     const handlePopoverOpenParking = event => {
       setParking(event.currentTarget);
     };
     const handlePopoverCloseParking = () => {
       setParking(null);
     };

     //RESTAURANT
     const handleChangeRestaurant = event => {
       setSpacing(Number(event.target.value));
     };
     const handlePopoverOpenRestaurant = event => {
       setRestaurant(event.currentTarget);
     };
     const handlePopoverCloseRestaurant = () => {
       setRestaurant(null);
     };

     //IS PRIVATE
     const handleChangeIsPrivate = event => {
       setSpacing(Number(event.target.value));
     };
     const handlePopoverOpenIsPrivate = event => {
       setIsPrivate(event.currentTarget);
     };
     const handlePopoverCloseIsPrivate = () => {
       setIsPrivate(null);
     }; 

     const preventDefault = event => event.preventDefault();

//------JSX------
	return(
	<React.Fragment>
	<Modal
		isOpen={props.showLearnMore}
		toggle={props.handleShowLearnMore}
	>
	<ModalHeader
		toggle={props.handleShowLearnMore}
		style={{backgroundColor:"#ff5722"}}
	> {post.name}
	</ModalHeader>

	<ModalBody>
		<Container>
			<Row
			className="text-center">
				<Col md={12}>
					<BeachAccessIcon />
				</Col>
			</Row>

			<Row
			className="text-center">
				<Col md={12}>
					<p id="learnMoredescription">{post.description}</p>
				</Col>
			</Row>
			<Row
			className="text-start">
				<Col>
					<p> <LocalOfferIcon /> ₱{post.price} </p>
					<p> <LocationOnIcon /> {post.address} </p>
					<Row >
						<Col md={6}>
						<KingBedIcon />&nbsp;
						<small>Rooms:</small> {post.noOfRooms}</Col>
						<Col md={6}>
						<PersonIcon />&nbsp;
						<small>Capacity:</small> {post.capacity}</Col>
					</Row>
				</Col>
			</Row>
      <Row
      className="text-start m-3">
      <Link 
        href={post.website}
        rel="noopener"
        onClick={preventDefault} 
        variant="body2">
       Right Click to visit their website
      </Link>
      </Row>
			<Accordion defaultActiveKey="1">
			<Row className="my-3">
				<Grid container justify="flex-start" direction="row">
                {post.pool === true ?
                              <Box>
                              <Typography
                                aria-owns={popOpenPool ? 'Pool' : undefined}
                                aria-haspopup="true"
                                onMouseEnter={handlePopoverOpenPool}
                                onMouseLeave={handlePopoverClosePool}
                              >
                              <PoolIcon color="secondary"/>
                              </Typography>
                              <Popover
                                id="Pool"
                                className={classes.popover}
                                classes={{
                                  paper: classes.paper,
                                }}
                                open={popOpenPool}
                                anchorEl={swimmingPool}
                                anchorOrigin={{
                                  vertical: 'bottom',
                                  horizontal: 'left',
                                }}
                                transformOrigin={{
                                  vertical: 'top',
                                  horizontal: 'left',
                                }}
                                onClose={handlePopoverClosePool}
                                disableRestoreFocus
                              >
                                <Typography>Swimming pool</Typography>
                              </Popover>
                              </Box>
                              :""}


                {post.karaoke === true ?
                                <Box>
                                <Typography
                                  aria-owns={popOpenKaraoke ? 'karaoke' : undefined}
                                  aria-haspopup="true"
                                  onMouseEnter={handlePopoverOpenKaraoke}
                                  onMouseLeave={handlePopoverCloseKaraoke}
                                >
                                <MicIcon color="secondary" />
                                </Typography>
                                <Popover
                                  id="karaoke"
                                  className={classes.popover}
                                  classes={{
                                    paper: classes.paper,
                                  }}
                                  open={popOpenKaraoke}
                                  anchorEl={karaoke}
                                  anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                  }}
                                  transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                  }}
                                  onClose={handlePopoverCloseKaraoke}
                                  disableRestoreFocus
                                >
                                  <Typography>Karaoke</Typography>
                                </Popover>
                                </Box>
                                :""}
                
               {post.jacuzzi === true ?
                                <Box>
                                <Typography
                                  aria-owns={popOpenJacuzzi ? 'jacuzzi' : undefined}
                                  aria-haspopup="true"
                                  onMouseEnter={handlePopoverOpenJacuzzi}
                                  onMouseLeave={handlePopoverCloseJacuzzi}
                                >
                                <HotTubIcon color="secondary" />
                                </Typography>
                                <Popover
                                  id="jacuzzi"
                                  className={classes.popover}
                                  classes={{
                                    paper: classes.paper,
                                  }}
                                  open={popOpenJacuzzi}
                                  anchorEl={jacuzzi}
                                  anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                  }}
                                  transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                  }}
                                  onClose={handlePopoverCloseJacuzzi}
                                  disableRestoreFocus
                                >
                                  <Typography>Jacuzzi</Typography>
                                </Popover>
                                </Box>
                                :""}

                
                {post.wifi === true ?
                              <Box>
                              <Typography
                                aria-owns={popOpenWifi ? 'wifi' : undefined}
                                aria-haspopup="true"
                                onMouseEnter={handlePopoverOpenWifi}
                                onMouseLeave={handlePopoverCloseWifi}
                                >
                                <WifiIcon color="secondary" />
                              </Typography>
                              <Popover
                                id="wifi"
                                className={classes.popover}
                                classes={{
                                  paper: classes.paper,
                                }}
                                open={popOpenWifi}
                                anchorEl={wifi}
                                anchorOrigin={{
                                  vertical: 'bottom',
                                  horizontal: 'left',
                                }}
                                transformOrigin={{
                                  vertical: 'top',
                                  horizontal: 'left',
                                }}
                                onClose={handlePopoverCloseWifi}
                                disableRestoreFocus
                                >
                                <Typography>Wifi </Typography>
                              </Popover>
                              </Box>
                              :""}


                {post.parking === true ?
                                <Box>
                                <Typography
                                  aria-owns={popOpenParking ? 'parking' : undefined}
                                  aria-haspopup="true"
                                  onMouseEnter={handlePopoverOpenParking}
                                  onMouseLeave={handlePopoverCloseParking}
                                >
                                  <LocalParkingIcon color="secondary" />
                                </Typography>
                                <Popover
                                  id="parking"
                                  className={classes.popover}
                                  classes={{
                                    paper: classes.paper,
                                  }}
                                  open={popOpenParking}
                                  anchorEl={parking}
                                  anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                  }}
                                  transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                  }}
                                  onClose={handlePopoverCloseParking}
                                  disableRestoreFocus
                                >
                                  <Typography>Parking</Typography>
                                </Popover>
                                </Box>
                                :""}

                {post.restaurant === true ?
                                <Box>
                                <Typography
                                  aria-owns={popOpenRestaurant ? 'restaurant' : undefined}
                                  aria-haspopup="true"
                                  onMouseEnter={handlePopoverOpenRestaurant}
                                  onMouseLeave={handlePopoverCloseRestaurant}
                                >
                                  <RestaurantIcon color="secondary" />
                                </Typography>
                                <Popover
                                  id="restaurant"
                                  className={classes.popover}
                                  classes={{
                                    paper: classes.paper,
                                  }}
                                  open={popOpenRestaurant}
                                  anchorEl={restaurant}
                                  anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                  }}
                                  transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                  }}
                                  onClose={handlePopoverCloseRestaurant}
                                  disableRestoreFocus
                                >
                                  <Typography>Restaurant</Typography>
                                </Popover>
                                </Box>
                                :""}

                {post.isPrivate === true ?
                                <Box>
                                <Typography
                                  aria-owns={popOpenIsPrivate ? 'isPrivate' : undefined}
                                  aria-haspopup="true"
                                  onMouseEnter={handlePopoverOpenIsPrivate}
                                  onMouseLeave={handlePopoverCloseIsPrivate}
                                >
                                  <SupervisorAccountIcon color="secondary" />
                                </Typography>
                                <Popover
                                  id="isPrivate"
                                  className={classes.popover}
                                  classes={{
                                    paper: classes.paper,
                                  }}
                                  open={popOpenIsPrivate}
                                  anchorEl={isPrivate}
                                  anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                  }}
                                  transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                  }}
                                  onClose={handlePopoverCloseIsPrivate}
                                  disableRestoreFocus
                                >
                                  <Typography>Private Resort</Typography>
                                </Popover>
                                </Box>
                                :""}
                  </Grid>
                  </Row>
            <Row className="my-3">      
			<Accordion.Toggle as={Button} variant="link" eventKey="0" color="primary"
				>Book now!
			</Accordion.Toggle>
			</Row>
			<Accordion.Collapse eventKey="0">
			<Row>
				<Booking
				post={post}
				sessionUser={props.sessionUser}
				/>
			</Row>
			</Accordion.Collapse>
			</Accordion>
		</Container>
	</ModalBody>
	</Modal>
	</React.Fragment>
		)
}
export default LearnMore;