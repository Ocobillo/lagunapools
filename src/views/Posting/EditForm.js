import React, {useState} from 'react';
import {
	Modal, ModalHeader,
	ModalBody, Button
 		} from 'reactstrap';
import {FormInput} from '../Components';

const EditForm = (props)=>{

const postToBeEdit = props.postToBeEdit;

//INITIATORS
const [editId, setEditId] = useState("");
const [showEditForm, setShowEditForm] = useState(false);

//HANDLES
const handleShowEditForm = (postId) =>{
      setShowEditForm(!showEditForm)
      setEditId(postId)
    }

//----JSX----
	return(
		<React.Fragment>
			<Modal
				isOpen={props.showEditForm}
				toggle={props.handleShowEditForm}
			>
			<ModalHeader
				toggle={props.handleShowEditForm}
				style={{backgroundColor:"pink"}}
				>EDIT POST
			</ModalHeader>

			<ModalBody>
				<FormInput 
					name={"name"}
					label={"Name of your place"}
					type={"text"}
					placeholder={postToBeEdit.name}
					required={props.nameRequired}
					onChange={props.handlenameChange}
				/>

				<FormInput 
					name={"address"}
					label={"Address"}
					type={"text"}
					placeholder={postToBeEdit.address}
					required={props.addressRequired}
					onChange={props.handleAddressChange}
				/>

				<FormInput 
					name={"city"}
					label={"City"}
					type={"text"}
					placeholder={postToBeEdit.city}
					required={props.cityRequired}
					onChange={props.handleCityChange}
				/>

				<FormInput 
					name={"price"}
					label={"Price"}
					type={"number"}
					placeholder={postToBeEdit.price}
					required={props.priceRequired}
					onChange={props.handlePriceChange}
				/>

				<FormInput 
					name={"summary"}
					label={"Summary"}
					type={"text"}
					placeholder={postToBeEdit.summary}
					required={props.summaryRequired}
					onChange={props.handleSummaryChange}
				/>

				<FormInput 
					name={"description"}
					label={"Description"}
					type={"text"}
					placeholder={postToBeEdit.description}
					required={props.descriptionRequired}
					onChange={props.handleDescriptionChange}
				/>

				<FormInput 
					name={"noOfRooms"}
					label={"Number of rooms"}
					type={"number"}
					placeholder={postToBeEdit.noOfRooms}
					required={props.noOfRoomsRequired}
					onChange={props.handleNoOfRoomsChange}
				/>

				<FormInput 
					name={"capacity"}
					label={"Capacity per room"}
					type={"number"}
					placeholder={postToBeEdit.capacity}
					required={props.capacityRequired}
					onChange={props.handleCapacityChange}
				/>
				<Button
					onClick={()=>props.handleEditPost(postToBeEdit)}
					>Edit
				</Button>			  
			</ModalBody>
			</Modal>
		</React.Fragment>
		)
}
export default EditForm;