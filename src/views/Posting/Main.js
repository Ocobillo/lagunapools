import React, {useEffect, useState} from 'react';   
import CardPost from './CardPost';
import AddForm from './AddForm';
import axios from 'axios';
import EditForm from './EditForm';
import LearnMore from './LearnMore';
import {Navi} from  '../Components';
import {Container, Button, Grid} from '@material-ui/core/';



//MAIN
const Main = (props)=>{

    //INTITIATORS
    let sessionUser = JSON.parse(sessionStorage.user);
    
    const [name, setname] = useState("");
    const [address, setAddress] = useState("");
    const [price, setPrice] = useState(0);
    const [summary, setSummary] = useState("");
    const [description, setDescription] = useState("");
    const [noOfRooms, setNoOfRooms] = useState(0);
    const [capacity, setCapacity] = useState(0);
    const [website, setWebsite] = useState("");
    const [checkedPool, setCheckedPool] = useState(false);
    const [checkedKaraoke ,setCheckedKaraoke] = useState(false);
    const [checkedJacuzzi ,setCheckedJacuzzi] = useState(false);
    const [checkedWifi ,setCheckedWifi] = useState(false);
    const [checkedParking ,setCheckedParking] = useState(false);
    const [checkedRestaurant ,setCheckedRestaurant] = useState(false);
    const [checkedIsPrivate ,setCheckedIsPrivate] = useState(false);

    const [nameRequired, setnameRequired] = useState(true);
    const [addressRequired, setAddressRequired] = useState(true);
    const [priceRequired, setPriceRequired] = useState(true);
    const [summaryRequired, setSummaryRequired] = useState(true);
    const [descriptionRequired, setDescriptionRequired] = useState(true);
    const [noOfRoomsRequired, setNoOfRoomsRequired] = useState(true);
    const [capacityRequired, setCapacityRequired] = useState(true);
    const [websiteRequired, setWebsiteRequired] = useState(true);
    const [isPoolChecked, setIsPoolChecked] = useState(false);

    //ADD FORM
    const [posts, setPosts] = useState([]);
    const [showForm, setShowForm] = useState(false);

    //ADD IMAGE
    const [imageToSave, setImage] = useState([]);
    const [infoMessage, setMessage] = useState("");
    const [imageUrls, setImageUrls] = useState([]);
    const [open, setOpen] = React.useState(false);

    //EDIT FORM
    const [showEditForm, setShowEditForm] = useState(false);
    const [postToBeEdit, setPostToBeEdit] = useState([]);

    //SHOWMORE
    const [showLearnMore, setShowLearnMore] = useState(false);
    const [postLearnMore, setPostLearnMore] = useState([]);

    //END OF INITIATORS

    //QUERIES
    useEffect(()=>{
      if(sessionStorage.token){
        axios.get('https://glacial-bayou-34265.herokuapp.com/showposts').then(res=>{  
            setPosts(res.data)
        })
      }else{
        window.location.replace('#/')
      }
    }, []);


    const selectImages = e => {
      let image = [e.target.files.item(0)];

      image = image.filter(image=>image.name.match(/\.(jpg|JPG|JPEG|jpeg|png|gif)$/))
      let imagepath = e.target.files.item(0).name
      let regex = new RegExp('[^.]+$');
      let extension = imagepath.match(regex)[0];

      if(extension === "jpg" || extension === "JPG" || extension === "JPEG" || 
         extension === "jpeg" || extension === "png" || extension === "gif") 
      {
        let message = "Valid image selected";
        setMessage(message);
      } else {
        let message = "Invalid image selected";
        setMessage(message);
      }

      setImage(image)
    }

    const handleClose = () => {
      setOpen(false);
    };

    const handleShowSaveConfirmation=()=>{
       setOpen(true);
       uploadImages();
    }


    const uploadImages = () => {
      let urls = [...imageUrls]
      const uploaders = imageToSave.map(image => {
        const data = new FormData();
        data.append("image", image, image.name)

        return axios.post('https://glacial-bayou-34265.herokuapp.com/upload', data).then(res => {
          urls.push(res.data.imageUrl)
          setImageUrls(urls)
        })
      })
    }


      const handleSavePost = ()=>{

      axios({
        method: "POST",
        url:"https://glacial-bayou-34265.herokuapp.com/addpost",
        data:{
          name:name,
          address:address,
          price:price,
          summary:summary,
          description:description,
          noOfRooms:noOfRooms,
          capacity:capacity,
          website:website,
          emailOfAgent:sessionUser.email,
          image: imageUrls[0],
          pool:checkedPool,
          karaoke:checkedKaraoke,
          jacuzzi:checkedJacuzzi,
          wifi:checkedWifi,
          parking:checkedParking,
          restaurant:checkedRestaurant,
          isPrivate:checkedIsPrivate
        }
      }).then(res=>{
        let newPosts = [...posts];
        newPosts.push(res.data);
        setPosts(newPosts);
      });
      handleRefresh();
      setOpen(false);
      setImageUrls([]);
    };



    const handleEditPost = (post) =>{
      axios({
        method: "PATCH",
        url: "https://glacial-bayou-34265.herokuapp.com/editpost/" + post._id,
        data: {
          name: post.name,
          address: post.address,
          price: post.price,
          summary: post.summary,
          description: post.description,
          noOfRooms: post.noOfRooms,
          capacity: post.capacity,
          emailOfAgent:sessionUser.email,
          image: post.capacity
        }
       }).then(res=>{
          let oldIndex;
               posts.forEach((post,index)=>{
                if(post._id == post._id){
               oldIndex = index;
           }
          });

        let newPosts = [...posts];
        newPosts.splice(oldIndex, 1, res.data);
        setPosts(newPosts);
        handleRefresh();
      })

    }

    const handleDeletePost = (postId)=>{
      axios.delete('https://glacial-bayou-34265.herokuapp.com/deletepost/'+postId)
      .then(res=>{
        let index = posts.findIndex(post=>post._id==postId);
        let newPosts = [...posts];
        newPosts.splice(index,1);
        setPosts(newPosts);
      })
    }



    //END OF QUERIES


    //HANDLES
    const handleRefresh = () => {
      setShowForm(false);
      setname("");
      setAddress("");
      setPrice("");
      setSummary("");
      setDescription("");
      setNoOfRooms("");
      setCapacity("");
      setnameRequired(true);
      setAddressRequired(true);
      setPriceRequired(true);
      setSummaryRequired(true);
      setDescriptionRequired(true);
      setNoOfRoomsRequired(true);
      setCapacityRequired(true);
      setPostToBeEdit([]);
      setCheckedPool(false)
      setCheckedKaraoke(false)
      setCheckedJacuzzi(false)
      setCheckedWifi(false)
      setCheckedParking(false)
      setCheckedRestaurant(false)
      setCheckedIsPrivate(false)
    }

    const handleShowAddForm = () =>{
      setShowForm(!showForm)
    };

    const handlenameChange = (e) => {
    if(e.target.value==""){
      setnameRequired(true);
      setname("");
    }else{
      setnameRequired(false)
      setname(e.target.value);
    }
  }

  const handleAddressChange = (e) => {
    if(e.target.value==""){
      setAddressRequired(true);
      setAddress("");
    }else{
      setAddressRequired(false)
      setAddress(e.target.value);
    }
  }


  const handlePriceChange = (e) => {
    if(e.target.value==""){
      setPriceRequired(true);
      setPrice(0);
    }else{
      setPriceRequired(false)
      setPrice(e.target.value);
    }
  }

  const handleSummaryChange = (e) => {
    if(e.target.value==""){
      setSummaryRequired(true);
      setSummary("");
    }else{
      setSummaryRequired(false)
      setSummary(e.target.value);
    }
  }

  const handleDescriptionChange = (e) => {
    if(e.target.value==""){
      setDescriptionRequired(true);
      setDescription("");
    }else{
      setDescriptionRequired(false)
      setDescription(e.target.value);
    }
  }

  const handleNoOfRoomsChange = (e) => {
    if(e.target.value==""){
      setNoOfRoomsRequired(true);
      setNoOfRooms(0);
    }else{
      setNoOfRoomsRequired(false)
      setNoOfRooms(e.target.value);
    }
  }

  const handleCapacityChange = (e) => {
    if(e.target.value==""){
      setCapacityRequired(true);
      setCapacity(0);
    }else{
      setCapacityRequired(false)
      setCapacity(e.target.value);
    }
  }

  const handleWebsiteChange = (e) => {
    if(e.target.value==""){
      setWebsiteRequired(true);
      setWebsite("");
    }else{
      setWebsiteRequired(false)
      setWebsite(e.target.value);
    }
  }

  const handleShowEditForm = (e)=>{
    setShowEditForm(!showEditForm);
    setPostToBeEdit(e);
  }

  const handleShowLearnMore = (post)=>{
    setShowLearnMore(!showLearnMore);
    setPostLearnMore(post);
  }

  const handlePoolChange = ()=>{
    setCheckedPool(!checkedPool);
  };

  const handleKaraokeChange = (yes)=>{
    setCheckedKaraoke(!checkedKaraoke);
  };

  const handleJacuzziChange = (yes)=>{
    setCheckedJacuzzi(!checkedJacuzzi)
  };

  const handleWifiChange = (yes)=>{
    setCheckedWifi(!checkedWifi)
  };

  const handleParkingChange = (yes)=>{
    setCheckedParking(!checkedParking)
  };

  const handleRestaurantChange = (yes)=>{
    setCheckedRestaurant(!checkedRestaurant)
  };

  const handleIsPrivateChange = (yes)=>{
    setCheckedIsPrivate(!checkedIsPrivate)
  };







  //END OF HANDLES

//----JSX----
    return(
    <React.Fragment>
      <Navi />
      <Container >

        {sessionUser.isAdmin ? 
        <Grid container justify="center">
        <Grid item>
        <Button
          variant="contained" 
          color="secondary"
          onClick={handleShowAddForm}
          className="m-3"
          >ADD NEW POST
        </Button>
        </Grid>
        </Grid>
        :""}

        <EditForm 
        showEditForm = {showEditForm}
        handleShowEditForm = {handleShowEditForm}
        postToBeEdit={postToBeEdit}
        handleEditPost={handleEditPost}
        />

        <LearnMore 
        showLearnMore={showLearnMore}
        postLearnMore={postLearnMore}
        handleShowLearnMore={handleShowLearnMore}
        sessionUser={sessionUser}
        />

        <AddForm 
        showForm={showForm}
        handleShowAddForm={handleShowAddForm}
        name={name}
        price={price}
        summary={summary}
        description={description}
        noOfRooms={noOfRooms}
        capacity={capacity}
        handleSavePost={handleSavePost}
        nameRequired={nameRequired}
        handlenameChange={handlenameChange}
        addressRequired={addressRequired}
        handleAddressChange={handleAddressChange}
        priceRequired={priceRequired}
        handlePriceChange={handlePriceChange}
        summaryRequired={summaryRequired}
        handleSummaryChange={handleSummaryChange}
        descriptionRequired={descriptionRequired}
        handleDescriptionChange={handleDescriptionChange}
        noOfRoomsRequired={noOfRoomsRequired}
        handleNoOfRoomsChange={handleNoOfRoomsChange}
        capacityRequired={capacityRequired}
        handleCapacityChange={handleCapacityChange}
        selectImages={selectImages}
        infoMessage={infoMessage}
        uploadImages={uploadImages}
        handleShowSaveConfirmation={handleShowSaveConfirmation}
        open={open}
        handleClose={handleClose}
        checkedPool={checkedPool}
        handlePoolChange={handlePoolChange}
        checkedKaraoke={checkedKaraoke}
        handleKaraokeChange={handleKaraokeChange}
        checkedJacuzzi={checkedJacuzzi}
        handleJacuzziChange={handleJacuzziChange}
        checkedWifi={checkedWifi}
        handleWifiChange={handleWifiChange}
        checkedParking={checkedParking}
        handleParkingChange={handleParkingChange}
        checkedRestaurant={checkedRestaurant}
        handleRestaurantChange={handleRestaurantChange}
        checkedIsPrivate={checkedIsPrivate}
        handleIsPrivateChange={handleIsPrivateChange}
        websiteRequired={websiteRequired}
        handleWebsiteChange={handleWebsiteChange}
        handleRefresh={handleRefresh}
        isPoolChecked={isPoolChecked}
        />

        <CardPost 
          key={posts._id}
          posts={posts} 
          handleDeletePost= {handleDeletePost}
          handleShowEditForm = {handleShowEditForm}
          handleShowLearnMore = {handleShowLearnMore}
        />
      </Container>
    </React.Fragment>
    )
}
export default Main;