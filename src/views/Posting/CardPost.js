import React from 'react';
import {  Grid, FormLabel, FormControlLabel,
          RadioGroup, Radio, Paper, Card,
          CardActionArea, CardActions, CardContent,
          CardMedia, Button, Typography,
          makeStyles, Box, Popover
                            } from "@material-ui/core";
import PoolIcon from '@material-ui/icons/Pool';                            
import MicIcon from '@material-ui/icons/Mic';
import HotTubIcon from '@material-ui/icons/HotTub';
import WifiIcon from '@material-ui/icons/Wifi';
import LocalParkingIcon from '@material-ui/icons/LocalParking';
import RestaurantIcon from '@material-ui/icons/Restaurant';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import {MouseOverPopover} from '../Components';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginTop: 2,
  },
  paper: {
    height: 140,
    width: 345,
  },
  control: {
    padding: theme.spacing(2),
  },
  card: {
    width: 345,
  },
  media: {
    height: 200,
  },
  popover: {
    pointerEvents: 'none',
  },
  paper: {
    padding: theme.spacing(1),
  },
}));

export default function CardPost(props) {
  const classes = useStyles();
  
//INITIATORS
  let posts = props.posts;
  const [spacing, setSpacing] = React.useState(2);
  const [swimmingPool, setSwimmingPool] = React.useState(null);
  const popOpenPool = Boolean(swimmingPool);
  const [karaoke, setKaraoke] = React.useState(null);
  const popOpenKaraoke = Boolean(karaoke);

  const [jacuzzi, setJacuzzi] = React.useState(null);
  const popOpenJacuzzi = Boolean(jacuzzi);

  const [wifi, setWifi] = React.useState(null);
  const popOpenWifi = Boolean(wifi);

  const [parking, setParking] = React.useState(null);
  const popOpenParking = Boolean(parking);

  const [restaurant, setRestaurant] = React.useState(null);
  const popOpenRestaurant = Boolean(restaurant);

  const [isPrivate, setIsPrivate] = React.useState(null);
  const popOpenIsPrivate = Boolean(isPrivate);
  
//HANDLERS

  //SWIMMING POOL
  const handleChangePool = event => {
    setSpacing(Number(event.target.value));
  };
  const handlePopoverOpenPool = event => {
    setSwimmingPool(event.currentTarget);
  };
  const handlePopoverClosePool = () => {
    setSwimmingPool(null);
  };

  //KARAOKE
  const handleChangeKaraoke = event => {
    setSpacing(Number(event.target.value));
  };
  const handlePopoverOpenKaraoke = event => {
    setKaraoke(event.currentTarget);
  };
  const handlePopoverCloseKaraoke = () => {
    setKaraoke(null);
  };

  //JACUZZI
  const handleChangeJacuzzi = event => {
    setSpacing(Number(event.target.value));
  };
  const handlePopoverOpenJacuzzi = event => {
    setJacuzzi(event.currentTarget);
  };
  const handlePopoverCloseJacuzzi = () => {
    setJacuzzi(null);
  };

  //WIFI
  const handleChangeWifi = event => {
    setSpacing(Number(event.target.value));
  };
  const handlePopoverOpenWifi = event => {
    setWifi(event.currentTarget);
  };
  const handlePopoverCloseWifi = () => {
    setWifi(null);
  };

  //PARKING
  const handleChangeParking = event => {
    setSpacing(Number(event.target.value));
  };
  const handlePopoverOpenParking = event => {
    setParking(event.currentTarget);
  };
  const handlePopoverCloseParking = () => {
    setParking(null);
  };

  //RESTAURANT
  const handleChangeRestaurant = event => {
    setSpacing(Number(event.target.value));
  };
  const handlePopoverOpenRestaurant = event => {
    setRestaurant(event.currentTarget);
  };
  const handlePopoverCloseRestaurant = () => {
    setRestaurant(null);
  };

  //IS PRIVATE
  const handleChangeIsPrivate = event => {
    setSpacing(Number(event.target.value));
  };
  const handlePopoverOpenIsPrivate = event => {
    setIsPrivate(event.currentTarget);
  };
  const handlePopoverCloseIsPrivate = () => {
    setIsPrivate(null);
  };


//------------------- JSX -------------------
  return (
<React.Fragment>    
    <Grid container className={classes.root} spacing={2}>
      <Grid item xs={12}>
        <Grid container justify="center" spacing={spacing}>
          {posts.map(post => (
            <Grid key={post._id} item>
            <Card className={classes.card}>
                  <CardActionArea 
                    onClick={()=>props.handleShowLearnMore(post)}
                  >

                    <CardMedia
                      className={classes.media}
                      image={post.image}
                      title={post.name}
                      alt="Not available"/>

                    <CardContent>
                      <Typography gutterBottom variant="h5" component="h2">
                        {post.name}
                      </Typography>
                      <Typography variant="h6" color="textSecondary" component="p">
                        PRICE: <strong id="cardPostPrice">{post.price}</strong>
                      </Typography>
                      <Typography variant="body2" color="textSecondary" component="p">
                        {post.summary}
                      </Typography>
                    </CardContent>
                  </CardActionArea>
                  <CardActions>
                    <Button 
                        size="small" 
                        color="primary"
                        className="mx-1" 
                        color="secondary"
                        variant="outlined"
                        onClick={()=>props.handleDeletePost(post._id)}
                        >
                      DELETE
                    </Button>

                    <Button size="small" color="primary"
                    className="mx-1" 
                    onClick={()=>props.handleShowLearnMore(post)}
                    variant="outlined"
                    > Reserve
                </Button>

                <Grid container justify="flex-start" direction="row">
                {post.pool === true ?
                              <Box>
                              <Typography
                                aria-owns={popOpenPool ? 'Pool' : undefined}
                                aria-haspopup="true"
                                onMouseEnter={handlePopoverOpenPool}
                                onMouseLeave={handlePopoverClosePool}
                              >
                              <PoolIcon color="secondary"/>
                              </Typography>
                              <Popover
                                id="Pool"
                                className={classes.popover}
                                classes={{
                                  paper: classes.paper,
                                }}
                                open={popOpenPool}
                                anchorEl={swimmingPool}
                                anchorOrigin={{
                                  vertical: 'bottom',
                                  horizontal: 'left',
                                }}
                                transformOrigin={{
                                  vertical: 'top',
                                  horizontal: 'left',
                                }}
                                onClose={handlePopoverClosePool}
                                disableRestoreFocus
                              >
                                <Typography>Swimming pool</Typography>
                              </Popover>
                              </Box>
                              :""}


                {post.karaoke === true ?
                                <Box>
                                <Typography
                                  aria-owns={popOpenKaraoke ? 'karaoke' : undefined}
                                  aria-haspopup="true"
                                  onMouseEnter={handlePopoverOpenKaraoke}
                                  onMouseLeave={handlePopoverCloseKaraoke}
                                >
                                <MicIcon color="secondary" />
                                </Typography>
                                <Popover
                                  id="karaoke"
                                  className={classes.popover}
                                  classes={{
                                    paper: classes.paper,
                                  }}
                                  open={popOpenKaraoke}
                                  anchorEl={karaoke}
                                  anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                  }}
                                  transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                  }}
                                  onClose={handlePopoverCloseKaraoke}
                                  disableRestoreFocus
                                >
                                  <Typography>Karaoke</Typography>
                                </Popover>
                                </Box>
                                :""}
                
               {post.jacuzzi === true ?
                                <Box>
                                <Typography
                                  aria-owns={popOpenJacuzzi ? 'jacuzzi' : undefined}
                                  aria-haspopup="true"
                                  onMouseEnter={handlePopoverOpenJacuzzi}
                                  onMouseLeave={handlePopoverCloseJacuzzi}
                                >
                                <HotTubIcon color="secondary" />
                                </Typography>
                                <Popover
                                  id="jacuzzi"
                                  className={classes.popover}
                                  classes={{
                                    paper: classes.paper,
                                  }}
                                  open={popOpenJacuzzi}
                                  anchorEl={jacuzzi}
                                  anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                  }}
                                  transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                  }}
                                  onClose={handlePopoverCloseJacuzzi}
                                  disableRestoreFocus
                                >
                                  <Typography>Jacuzzi</Typography>
                                </Popover>
                                </Box>
                                :""}

                
                {post.wifi === true ?
                              <Box>
                              <Typography
                                aria-owns={popOpenWifi ? 'wifi' : undefined}
                                aria-haspopup="true"
                                onMouseEnter={handlePopoverOpenWifi}
                                onMouseLeave={handlePopoverCloseWifi}
                                >
                                <WifiIcon color="secondary" />
                              </Typography>
                              <Popover
                                id="wifi"
                                className={classes.popover}
                                classes={{
                                  paper: classes.paper,
                                }}
                                open={popOpenWifi}
                                anchorEl={wifi}
                                anchorOrigin={{
                                  vertical: 'bottom',
                                  horizontal: 'left',
                                }}
                                transformOrigin={{
                                  vertical: 'top',
                                  horizontal: 'left',
                                }}
                                onClose={handlePopoverCloseWifi}
                                disableRestoreFocus
                                >
                                <Typography>Wifi </Typography>
                              </Popover>
                              </Box>
                              :""}


                {post.parking === true ?
                                <Box>
                                <Typography
                                  aria-owns={popOpenParking ? 'parking' : undefined}
                                  aria-haspopup="true"
                                  onMouseEnter={handlePopoverOpenParking}
                                  onMouseLeave={handlePopoverCloseParking}
                                >
                                  <LocalParkingIcon color="secondary" />
                                </Typography>
                                <Popover
                                  id="parking"
                                  className={classes.popover}
                                  classes={{
                                    paper: classes.paper,
                                  }}
                                  open={popOpenParking}
                                  anchorEl={parking}
                                  anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                  }}
                                  transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                  }}
                                  onClose={handlePopoverCloseParking}
                                  disableRestoreFocus
                                >
                                  <Typography>Parking</Typography>
                                </Popover>
                                </Box>
                                :""}

                {post.restaurant === true ?
                                <Box>
                                <Typography
                                  aria-owns={popOpenRestaurant ? 'restaurant' : undefined}
                                  aria-haspopup="true"
                                  onMouseEnter={handlePopoverOpenRestaurant}
                                  onMouseLeave={handlePopoverCloseRestaurant}
                                >
                                  <RestaurantIcon color="secondary" />
                                </Typography>
                                <Popover
                                  id="restaurant"
                                  className={classes.popover}
                                  classes={{
                                    paper: classes.paper,
                                  }}
                                  open={popOpenRestaurant}
                                  anchorEl={restaurant}
                                  anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                  }}
                                  transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                  }}
                                  onClose={handlePopoverCloseRestaurant}
                                  disableRestoreFocus
                                >
                                  <Typography>Restaurant</Typography>
                                </Popover>
                                </Box>
                                :""}

                {post.isPrivate === true ?
                                <Box>
                                <Typography
                                  aria-owns={popOpenIsPrivate ? 'isPrivate' : undefined}
                                  aria-haspopup="true"
                                  onMouseEnter={handlePopoverOpenIsPrivate}
                                  onMouseLeave={handlePopoverCloseIsPrivate}
                                >
                                  <SupervisorAccountIcon color="secondary" />
                                </Typography>
                                <Popover
                                  id="isPrivate"
                                  className={classes.popover}
                                  classes={{
                                    paper: classes.paper,
                                  }}
                                  open={popOpenIsPrivate}
                                  anchorEl={isPrivate}
                                  anchorOrigin={{
                                    vertical: 'bottom',
                                    horizontal: 'left',
                                  }}
                                  transformOrigin={{
                                    vertical: 'top',
                                    horizontal: 'left',
                                  }}
                                  onClose={handlePopoverCloseIsPrivate}
                                  disableRestoreFocus
                                >
                                  <Typography>Private Resort</Typography>
                                </Popover>
                                </Box>
                                :""}
                  </Grid>
                  </CardActions>
                </Card>
            </Grid>
          ))}
        </Grid>
      </Grid>
    </Grid>
</React.Fragment>      
  );
}

                    // <Button size="small" color="primary"
                    //   outline color="primary"
                    //   onClick={()=>props.handleShowEditForm(post)}
                    //   >Edit
                    // </Button>