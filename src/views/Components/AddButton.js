import React from 'react';
import{
	Button,
	Container,
	Row,
	Col
} from 'reactstrap';

const AddButton = (onClick, ...props)=>{
	return(
		<React.Fragment>
		<Container>
			<Row>
				<Col 
				xs={{size:2, order:2, offset:10}}
				sm={{size:3, order:2, offset:9}}
				md={{size:3, order:2, offset:9}}
				lg={{size:2, order:2, offset:10}}
				>
					<Button color="success"
					onClick="{onClick}"
					> NEW POST
					</Button>
				</Col>
			</Row>
		</Container>
		</React.Fragment>
		)}

export default AddButton;