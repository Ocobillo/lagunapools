import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import { Menu, MenuItem, FormGroup,
         FormControlLabel, Switch, Typography,
         IconButton, Toolbar, AppBar,
         makeStyles, Button                     
                                   } from '@material-ui/core';

const Navi = ()=> {

  //MATERIAL UI STYLING
     const useStyles = makeStyles(theme => ({
      root: {
        flexGrow: 1,
      },
      menuButton: {
        marginRight: theme.spacing(2),
      },
      title: {
        flexGrow: 1,
      },
    }));

//SESSION LOGIN

//INITIATORS
  const classes = useStyles();
  const [auth, setAuth] = useState(false);
  const [anchorEl, setAnchorEl] =useState(null);
  const [user, setUser] = useState("");

  const open = Boolean(anchorEl);



 useEffect(()=>{
  if(sessionStorage.token){
    let user = JSON.parse(sessionStorage.user);
    setUser(user);
    setAuth(true);
  }
 }, [])

 const handleLogout =()=>{
  sessionStorage.clear()
  setAuth(false)
  window.location.replace('#/login');
 }


  const handleMenu = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

 const handleAdminRoute =()=>{
   window.location.replace('#/admin');
 }

 const handleMainRoute=()=>{
  window.location.replace('#/main');
 }


//JSX
    return ( 
    <React.Fragment>  

 <AppBar position="static" variant="primary">
   <Toolbar>
     <Typography className={classes.title} onClick={handleMainRoute}>
       <button id="NaviTitleButton">Laguna Pools</button>
     </Typography>
     {auth && (
       <div>
         <IconButton
           aria-label="account of current user"
           aria-controls="menu-appbar"
           aria-haspopup="true"
           onClick={handleMenu}
           color="inherit"
         >
           <AccountCircle />
         </IconButton>
         <Menu
           id="menu-appbar"
           anchorEl={anchorEl}
           anchorOrigin={{
             vertical: 'top',
             horizontal: 'right',
           }}
           keepMounted
           transformOrigin={{
             vertical: 'top',
             horizontal: 'right',
           }}
           open={open}
           onClose={handleClose}
         >
           {user.isAdmin?<MenuItem onClick={handleMainRoute}>Main Page</MenuItem>:""}
           {user.isAdmin?<MenuItem onClick={handleAdminRoute}>Admin Tool</MenuItem>:""}
           <MenuItem onClick={handleLogout}>Logout</MenuItem>
         </Menu>
       </div>
     )}
   </Toolbar>
 </AppBar>
    </React.Fragment>
    )
}
export default Navi;