import React from 'react';
import {Grid, Box} from '@material-ui/core'
const Footer = ()=>{

    return(
        <React.Fragment>
        <Grid container>
	        <Grid 
	        	container 
	        	id='footer'
	        	justify='space-around'>
		        <Grid item md={4}>
			        <Box className='text-center'>
			        	<p className="footerPage">
			        	Contact Us<br /> GLOBE: +6391788888</p>
			        </Box>
		        </Grid>
		        <Grid item md={4}>
		        	<Box>
		        	<p>This website is for educational purposes only</p>
		        	</Box>
		        </Grid>
		        <Grid item md={4}>
		        	<Box>
		        	<p>ABOUT US<br />
	This website's aim is to make people see the wonders of Laguna pools and how
		        	easy it is to book one.</p>
		        	</Box>
		        </Grid>
	        </Grid>
        </Grid>
        </React.Fragment>
    )
}
export default Footer;