import React from 'react';
import {Elements, StripeProvider} from 'react-stripe-elements';
import CheckoutForm2 from './CheckoutForm2';

const Payments = () => {
	return (
		<React.Fragment>
	
		<StripeProvider apiKey="pk_test_7VJYwxE5OIKcX4vnZs9XIYER00Lj5bmdSb">
			<div className="payment-container">

				<div className="payment d-flex justify-content-center">
					<Elements>
						<CheckoutForm2 />
					</Elements>
				</div>
			</div>
		</StripeProvider>

		</React.Fragment>
	)
}

export default Payments;