import React from 'react';
import {CardElement, injectStripe} from 'react-stripe-elements';
import axios from 'axios';
import Swal from 'sweetalert2';

const CheckoutForm = (props) => {
		// CommonJS
	const Swal = require('sweetalert2')

	const submit = async (e) => {
		let user = JSON.parse(sessionStorage.user);
		console.log(user.email);
		console.log(props.toPay);
		// console.log(e)
		let {token} = await props.stripe.createToken({name:"Name"})

		axios.post('https://glacial-bayou-34265.herokuapp.com/charge',{
			email: user.email,
			amount: (props.toPay)

		}).then(Swal.fire({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, Pay Now!'
		}).then((result) => {
			if (result.value) {
				Swal.fire(
					'Success!',
					'Your booking has been booked.',
					'success'
					)
			}
		}));

		// let response = await fetch('http://localhost:4000/charge', {
		// 	method: "POST",
		// 	headers: {"Content-type":"text-plain"},
		// 	body: token.id
		// });

		// console.log(response)

		// if (response.ok) console.log("Purchase Complete");
	}

	return (
		<div className="checkout text-center">
			<h5> P{props.toPay}.00</h5>
	       	<p className="py-3">Would you like to complete the payment?</p>
        	<CardElement py-2 />
        	<button 
        	className="btn btn-primary my-3 col-lg-12" 
        	style={{"backgroundColor":"#fd79a8", "color": "#fff","border": "1px solid #fd79a8"}}
        	onClick={submit}>Pay Now</button>
      	</div>
	)
}

export default injectStripe(CheckoutForm);



