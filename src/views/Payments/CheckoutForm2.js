import React from 'react';
import {CardElement, injectStripe} from 'react-stripe-elements';
import moment from 'moment';
import axios from 'axios'

const CheckoutForm = (props) => {

	const submit = async (e) => {
        // console.log(e)
        
        let {token} = await props.stripe.createToken({
            name:"Name"})
        let tokenId = token.id
        let amount = 30000

        let data = new FormData;
        data.append('amount', 12345)

		axios.post('https://glacial-bayou-34265.herokuapp.com/charge',{
            amount: props.totalPayment,
            source: token.id
        }).then(res=>{


            
            axios({
                method: "POST",
                url: "https://glacial-bayou-34265.herokuapp.com/addbooking",
                data: {

                }
              }).then(res => {
                console.log(res.data);
              });
            })
        
	}

	return (
		<div className="checkout">
			<p>Amount: {props.totalPayment}</p>
        	<p>Would you like to complete the purchase?</p>
        	<CardElement />
        	<button onClick={submit}>Book</button>
      	</div>
	)
}

export default injectStripe(CheckoutForm);