import React, {useState} from 'react';
import { Jumbotron } from 'reactstrap';
import {Button} from '@material-ui/core';

const Example = (props) => {

const [modal, setModal] = useState(false);


const handleRouteToRegister =()=>{
  window.location.replace('#/register')
}

const handleRouteToLogin =()=>{
  window.location.replace('#/login')
}

//------------JSX---------------
  return (
    <div>
      <Jumbotron id="jumbotronbg">
        <h1 className="display-3" id="titleP">Laguna Pools</h1>
        <p className="lead">The quickest way to get the best pool in 
        <strong id="pansolLaguna"> Pansol Laguna.</strong></p>
        <p className="homePageDescription">This is the best place to find the perfect resort for your friends and family.</p>
        <p className="homePageDescription">We can give you a place for your team building and family events. BOOK NOW!</p>
        <p className="lead">
          <Button 
            variant="contained" 
            color="primary" 
            id="btnf1"
            onClick={handleRouteToRegister}
            >Register
          </Button>

          <Button 
            variant="contained" 
            color="primary" 
            id="btnf2"
            onClick={handleRouteToLogin}
            >Login
          </Button>
        </p>
      </Jumbotron>
    </div>
  );
};

export default Example;