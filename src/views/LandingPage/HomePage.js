import React, {Fragment} from 'react';
import Jumbotron from './Jumbotron';
import Description from './Description';
import Cardlist from './Cardlist';
import {Footer} from '../Components';

const HomePage =()=>{


	return(
	<Fragment>
		<Jumbotron />
		<Description />
		<Cardlist />
		<Footer />
	</Fragment>
		)
}
export default HomePage;