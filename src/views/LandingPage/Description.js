import React, {Fragment} from 'react';
import {List, ListItem, ListItemAvatar, Avatar, 
        makeStyles, ListItemText
} from '@material-ui/core';
import FastForwardIcon from '@material-ui/icons/FastForward';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import PaymentIcon from '@material-ui/icons/Payment';
import PhoneAndroidIcon from '@material-ui/icons/PhoneAndroid';
import AccessTimeIcon from '@material-ui/icons/AccessTime';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
}));


const Description =()=>{
	const classes = useStyles();

	return(
	<Fragment>
	<List className={classes.root}>
    <strong id="descriptionpagetitle">Some of the benefits you get when you book through our portal:</strong>
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <FastForwardIcon color="secondary" />
            </Avatar>
          </ListItemAvatar>
          <ListItemText 
          primary="Quick view" 
          secondary="No need to go to a lot of websites just to see the updated packages of Laguna resorts" />
        </ListItem>
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <ThumbUpIcon color="secondary" />
            </Avatar>
          </ListItemAvatar>
          <ListItemText 
          primary="Credible" 
          secondary="You can trust that all our listed pools are assessed and legit" />
        </ListItem>
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <PaymentIcon color="secondary"/>
            </Avatar>
          </ListItemAvatar>
          <ListItemText 
          primary="Affordable" 
          secondary="You can get speacial discounts and offers" />
        </ListItem>
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <PhoneAndroidIcon color="secondary"/>
            </Avatar>
          </ListItemAvatar>
          <ListItemText 
          primary="Fast customer service" 
          secondary="You can chat with our customer service agents for your inquiries and reservation needs" />
        </ListItem>
        <ListItem>
          <ListItemAvatar>
            <Avatar>
              <AccessTimeIcon color="secondary"/>
            </Avatar>
          </ListItemAvatar>
          <ListItemText 
          primary="Save time" 
          secondary="No need to call and wait on queue to make your reservation" />
        </ListItem>
      </List>
	</Fragment>
		)
}
export default Description