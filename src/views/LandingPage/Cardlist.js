import React, {Fragment} from 'react';
import {Grid, Card, Paper, makeStyles,
		CardMedia, Box
} from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    height: 140,
    width: 300,
    margin: 7,
  },
  control: {
    padding: theme.spacing(2),
  },
  media: {
    height: 140,
  },
}));

const Cardlist =()=>{
	const [spacing, setSpacing] = React.useState(2);
	const classes = useStyles();

	return(
	<Fragment>
	<Grid container>
	<Grid 
		container 
		direction="row" 
		className={classes.root} 
		justify='center'
		spacing={2}
		id="gridBox">
		<Box className={classes.paper}>
			<Card>
			<Box
			  className={classes.media}
			  id="dotLogo"
			  title="DOT logo"
			/>
			</Card>
		</Box>

		<Box className={classes.paper}>
			<Card>
			<Box
			  className={classes.media}
			  id="dtiLogo"
			  title="Contemplative Reptile"
			/>
			</Card>
		</Box>

		<Box className={classes.paper}>
			<Card>
			<Box
			  className={classes.media}
			  id="lagunaLogo"
			  title="Contemplative Reptile"
			/>
			</Card>
		</Box>
	</Grid>
	</Grid>        
	</Fragment>
		)
}
export default Cardlist;