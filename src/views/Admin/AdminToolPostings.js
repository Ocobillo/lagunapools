import React, {useState, useEffect} from 'react';
import { CSVLink, CSVDownload } from "react-csv";
import axios from 'axios';
import { makeStyles, Table,
          TableBody, TableCell,
          TableHead, TableRow,
          Paper, Button
            } from '@material-ui/core';


//MATERIAL-UI STYLING
const useStyles = makeStyles({
  root: {
    width: '100%',
    overflowX: 'auto',
    margin: "none",
    padding: "none",
  },
  table: {
    minWidth: 650,
    margin: "none",
    padding: "none",
  },
  tHeader: {
    background: '#F7971E',
    backgroundColor: '#89d8d3',
    backgroundImage: 'linear-gradient(315deg, #89d8d3 0%, #03c8a8 74%)',
  },
});

const AdminToolPostings =()=> {

//MATERIAL UI
  const classes = useStyles();

//INITIATORS
	const [posts, setPosts] = useState([]);

//QUERIES
	useEffect(()=>{
        axios.get('https://glacial-bayou-34265.herokuapp.com/showposts').then(res=>{
            setPosts(res.data)
        })
    }, [])



  return (
<React.Fragment>

    <div className="container">
      <div className="row justify-content-end m-2">
        <CSVLink data={posts}>
          <Button
            variant="contained"
            color="primary"
              >DOWNLOAD TO CSV
          </Button>
        </CSVLink>
      </div>
    </div>

    <Paper className={classes.root}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow className={classes.tHeader}>
            <TableCell align="center">RESORT NAME</TableCell>
            <TableCell align="center">PRICE</TableCell>
            <TableCell align="center">ADDRESS</TableCell>
            <TableCell align="center">NO. OF ROOMS</TableCell>
            <TableCell align="center">CAPACITY</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {posts.map(post => (
            <TableRow key={post._id}>
              <TableCell component="th" scope="row" align="center">
                {post.name}
              </TableCell>
              <TableCell align="center">{post.price}</TableCell>
              <TableCell align="center">{post.address} {post.city}</TableCell>
              <TableCell align="center">{post.noOfRooms}</TableCell>
              <TableCell align="center">{post.capacity}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>   
</React.Fragment>      
  );
}
export default AdminToolPostings;