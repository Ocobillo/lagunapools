import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import AdminToolPostings from './AdminToolPostings';
import AdminToolUsers from './AdminToolUsers';
import AdminToolBookings from './AdminToolBookings';
import {Navi} from '../Components/';


const TabPanel=(props)=> {
  const { children, value, index, ...other } = props;

  return (
    <Typography
      component="div"
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      <Box p={3}>{children}</Box>
    </Typography>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

const a11yProps=(index)=>{
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    

  },
}));

const Table=()=>{
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
<React.Fragment>
    <Navi />
    <div className={classes.root}>
      <AppBar position="static" color="primary">
        <Tabs value={value} onChange={handleChange} aria-label="simple tabs example"
         justify="center"
        >
          <Tab label="USERS" {...a11yProps(0)} 
            
            xs={12}
          />
          <Tab label="POSTINGS" {...a11yProps(1)} 
            
            xs={12}
          />
          <Tab label="BOOKINGS" {...a11yProps(2)} 
            
            xs={12}
          />

        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <AdminToolUsers />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <AdminToolPostings />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <AdminToolBookings />
      </TabPanel>
    </div>
</React.Fragment>    
  );
}
export default Table;