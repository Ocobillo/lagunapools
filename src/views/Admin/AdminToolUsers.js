import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {makeStyles,
        Table, TableBody, TableCell,
        TableHead, TableRow, Paper,
        Button, Dialog, DialogActions,
        DialogContent,
        DialogTitle, InputLabel, Input, 
        MenuItem, FormControl, Select, TextField
                                        } from '@material-ui/core';
import DeleteRoundedIcon from '@material-ui/icons/DeleteRounded';
import { CSVLink } from "react-csv";


//MATERIAL-UI STYLING
const useStyles = makeStyles(theme => ({
  root2: {
    width: '100%',
    overflowX: 'auto',
    margin: "none",
    padding: "none",

  },
  table: {
    minWidth: 650,
    margin: "none",
    padding: "none",
  },

  tHeader2: {
  	background: '#F7971E',
    backgroundColor: '#89d8d3',
    backgroundImage: 'linear-gradient(315deg, #89d8d3 0%, #03c8a8 74%)',
  },

  deletebtn: {
    margin: 3,
  },

  editbtn: {
    margin: 3,
  },
    container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },

}));


//---------------------------------------
const AdminToolUsers =()=> {

//MATERIAL UI
  const classes = useStyles();

//INITIATORS
	const [users, setUsers] = useState([]);
  const [open, setOpen] = useState(false);
  const [placeholder, setPlaceHolder] = useState('');
  const [editFirstName, setEditFirstName] = useState('');
  const [editLastName, setEditLastName] = useState('');
  const [editEmail, setEditEmail] = useState('');
  const [editContactNumber, setEditContactNumber] = useState(0);
  const [editAccess, setEditAccess] = useState('');

  const [editFirstNameRequired, setEditFirstNameRequired] = useState(true);
  const [editLastNameRequired, setEditLastNameRequired] = useState(true);
  const [editEmailRequired, setEditEmailRequired] = useState(true);
  const [editContactNumberRequired, setEditContactNumberRequired] = useState(true);
  const [editAccessRequired, setEditAccessRequired] = useState(true);



//QUERIES
	useEffect(()=>{
    if(sessionStorage.token){
        axios.get('https://glacial-bayou-34265.herokuapp.com/showusers').then(res=>{
            setUsers(res.data)
        })
      }else{
        window.location.replace('#/')
      }
    }, []);

  const QueryEditUser = (userId, password)=>{
    let samepassword = password;

    axios({
      method: "PATCH",
      url: "https://glacial-bayou-34265.herokuapp.com/editpost/" + userId,
      data: {
        firstName: editFirstName,
        lastName: editLastName,
        email: editEmail,
        contactNumber: editContactNumber,
        password: samepassword,
        access: editAccess
      }
    }).then(res=>{
      let old;
      users.forEach((user, index)=>{
        if(user._id === userId){
          old = index;
        }
      });
      let newUsers = [...users];
      newUsers.splice(old, 1, res.data);
      setUsers(newUsers);
    })
  }

  const handleDeleteUser = (userId)=>{
    console.log("Hello");
    console.log(userId);
    axios.delete('https://glacial-bayou-34265.herokuapp.com/deleteuser/'+userId)
    .then(res=>{
      let index = users.findIndex(user=>user._id=userId);
      let newUsers = [...users];
      newUsers.splice(index, 1);
      setUsers(newUsers);
    })

    handleRefresh();

  }

//HANDLERS

  const handleRefresh=()=>{

  }

  const handleEditFirstName = e => {
    if(e === "") {
      setEditFirstNameRequired(true);
      setEditFirstName("");
    }else{
      setEditFirstNameRequired(false)
      setEditFirstName(e );
    }
  };

  const handleEditLastName = e => {
    if(e === "") {
      setEditLastNameRequired(true);
      setEditLastName("");
    }else{
      setEditLastNameRequired(false)
      setEditLastName(e );
    }
  };

  const handleEditEmail = e => {
    if(e === "") {
      setEditEmailRequired(true);
      setEditEmail("");
    }else{
      setEditEmailRequired(false)
      setEditEmail(e);
    }
  };

  const handleEditContactNumber = e => {
    if(e === "") {
      setEditContactNumberRequired(true);
      setEditContactNumber(0);
    }else{
      setEditContactNumberRequired(false)
      setEditContactNumber(e);
    }
  };

  const handleEditAccess = e => {
    console.log(e)
    if(e === "") {
      setEditAccessRequired(true);
      setEditAccess("");
    }else{
      setEditAccessRequired(false)
      setEditAccess(e );
    }
  };

  const handleClickOpen = (e) => {
    setOpen(true);
    setPlaceHolder(e);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
<React.Fragment>
<div className="container">
  <div className="row justify-content-end m-2">
    <CSVLink data={users}>
      <Button
        variant="contained"
        color="primary"
          >DOWNLOAD TO CSV
      </Button>
    </CSVLink>
  </div>
</div>


    <Paper className={classes.root2}>
      <Table className={classes.table} aria-label="caption table">
        <TableHead>
          <TableRow className={classes.tHeader2}>
            <TableCell align="center">NAME</TableCell>
            <TableCell align="center">EMAIL</TableCell>
            <TableCell align="center">CONTACT NUMBER</TableCell>
            <TableCell align="center">ACCESS</TableCell>
            <TableCell align="center">ACTION</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {users.map(user => (
            <TableRow key={user._id}>
              <TableCell component="th" scope="row" align="center">
                {user.firstName} {user.lastName}
              </TableCell>
              <TableCell 
                  align="center"
                  >{user.email}
              </TableCell>
              <TableCell 
                  align="center"
                  >{user.contactNumber}
              </TableCell>
              <TableCell
                  align="center"
                  >{user.isAdmin ? "Admin": "Customer"}
              </TableCell>
              <TableCell 
                  align="center">
                  <Dialog   open={open} onClose={handleClose}>
                    <DialogTitle
                    >Do you want to change {user.firstName} {user.lastName} access?
                    </DialogTitle>
                    <DialogContent>
                      <form 
                      className={classes.container}
                      >
                        <TextField 
                        id="standard-basic" 
                        label="First Name"
                        onChange={()=>handleEditFirstName(user.firstName)}
                        fullWidth

                        />

                        <TextField 
                        id="standard-basic" 
                        label="Last Name"
                        onChange={()=>handleEditLastName(user.lastName)}
                        fullWidth

                        />

                        <TextField 
                        id="standard-basic" 
                        label="Email"
                        onChange={()=>handleEditEmail(user.email)}
                        fullWidth

                        />

                        <TextField 
                        id="standard-basic" 
                        label="Contact Number"
                        onChange={()=>handleEditContactNumber(user.contactNumber)}
                        fullWidth

                        />

                        <FormControl 
                          className={classes.formControl}
                          >
                            <InputLabel 
                            id="demo-dialog-select-label"
                            >{placeholder}
                            </InputLabel>
                            <Select
                            value={editAccess}
                            onChange={handleEditAccess}
                            input={<Input />}
                            fullWidth>
                                <MenuItem value={"user"}
                                    onClick={()=>handleEditAccess("user")}
                                    >User</MenuItem>
                                <MenuItem value={"agent"}
                                    onClick={()=>handleEditAccess("agent")}
                                    >Resort agent</MenuItem>
                                <MenuItem value={"admin"}
                                    onClick={()=>handleEditAccess("admin")}
                                    >Admin</MenuItem>
                            </Select>
                        </FormControl>

                      </form>
                    </DialogContent>
                    <DialogActions>
                    <Button onClick={handleClose} color="primary">
                    Cancel
                    </Button>
                    <Button onClick={()=>QueryEditUser(user._id, user.password)} color="primary">
                    Ok
                    </Button>
                    </DialogActions>
                  </Dialog>
                  <Button
                  variant="contained"
                  color="secondary"
                  className={classes.deletebtn}
                  startIcon={<DeleteRoundedIcon 
                    onClick={()=>handleDeleteUser(user._id)}
                    />}
                  >Del
                  </Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>   
</React.Fragment>      
  );
}
export default AdminToolUsers;