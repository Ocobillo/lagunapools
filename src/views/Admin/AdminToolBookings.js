import React, {useState, useEffect} from 'react';
import axios from 'axios';
import {makeStyles,
        Table, TableBody, TableCell,
        TableHead, TableRow, Paper,
        Button, Dialog, DialogActions,
        DialogContent,
        DialogTitle, InputLabel, Input, 
        MenuItem, FormControl, Select, TextField
                                        } from '@material-ui/core';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/styles';
import DeleteRoundedIcon from '@material-ui/icons/DeleteRounded';
import EditTwoToneIcon from '@material-ui/icons/EditTwoTone';
import { CSVLink, CSVDownload } from "react-csv";


//MATERIAL-UI STYLING
const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    overflowX: 'auto',
    margin: "none",
    padding: "none",

  },
  table: {
    minWidth: 650,
    margin: "none",
    padding: "none",
  },

  tHeader2: {
  	background: '#F7971E',
    backgroundColor: '#89d8d3',
    backgroundImage: 'linear-gradient(315deg, #89d8d3 0%, #03c8a8 74%)',
  },

  deletebtn: {
    margin: 3,
  },

  editbtn: {
    margin: 3,
  },
    container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  title: {
    fontSize: 12,
  }
}));


//---------------------------------------
const AdminToolUsers =()=> {

//MATERIAL UI
  const classes = useStyles();


//INITIATORS
  let sessionUser = JSON.parse(sessionStorage.user);

	const [bookings, setBookings] = useState([]);
  const [open, setOpen] = useState(false);



//QUERIES
	useEffect(()=>{
        axios.get('https://glacial-bayou-34265.herokuapp.com/showbookings').then(res=>{
            setBookings(res.data)
        })
    }, [])


//HANDLERS



  return (
<React.Fragment>
<div className="container">
  <div className="row justify-content-end m-2">
    <CSVLink data={bookings}>
      <Button
        variant="contained"
        color="primary"
          >DOWNLOAD TO CSV
      </Button>
    </CSVLink>
  </div>
</div>


    <Paper className={classes.root}>
      <Table className={classes.table} aria-label="caption table">
        <TableHead>
          <TableRow className={classes.tHeader2}>
            <TableCell align="center" className={classes.title}>CUSTOMER EMAIL</TableCell>
            <TableCell align="center" className={classes.title}>RESORT NAME</TableCell>
            <TableCell align="center" className={classes.title}>ADDRESS</TableCell>
            <TableCell align="center" className={classes.title}>ATTENDEES</TableCell>
            <TableCell align="center" className={classes.title}>START DATE</TableCell>
            <TableCell align="center" className={classes.title}>END DATE</TableCell>
            <TableCell align="center" className={classes.title}>NUMBER OF DAYS</TableCell>
            <TableCell align="center" className={classes.title}>TOTAL PAYMENTS</TableCell>
            <TableCell align="center" className={classes.title}>PAYMENT STATUS</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {bookings.map(booking => (
            <TableRow key={booking._id}>
              <TableCell component="th" scope="row" align="center">
                {booking.emailOfUser}
              </TableCell>
              <TableCell 
                  align="center"
                  >{booking.name}
              </TableCell>
              <TableCell 
                  align="center"
                  >{booking.address}
              </TableCell>
              <TableCell
                  align="center"
                  >{booking.noOfAttendees}
              </TableCell>
              <TableCell 
                  align="center"
                  >{booking.dateFrom}
              </TableCell>
              <TableCell 
                  align="center"
                  >{booking.dateTo}
              </TableCell>
              <TableCell 
                  align="center"
                  >{booking.noOfDays}
              </TableCell>
              <TableCell 
                  align="center"
                  >{booking.totalPayment}
              </TableCell>
              <TableCell 
                  align="center"
                  >{booking.isPaid ? "Paid":"Not Paid"}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Paper>   
</React.Fragment>      
  );
}
export default AdminToolUsers;