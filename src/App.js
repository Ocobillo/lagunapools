import React from 'react';
import logo from './logo.svg';
import './App.css';
import { makeStyles } from '@material-ui/core/styles';
import CircularProgress from '@material-ui/core/CircularProgress';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { HashRouter, Route,
        Switch
            } from 'react-router-dom';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    '& > * + *': {
      marginLeft: theme.spacing(2),
    },
  },
}));


const Loading =()=>{
const classes = useStyles();
  return (
    <React.Fragment>
    <Grid container justify="center" alignItems="center">
      <Grid item >
        <CircularProgress color="secondary" />
      </Grid>
    </Grid>
    </React.Fragment>
  );
}

//Routers
const Main = React.lazy(()=>import('./views/Posting/Main'));
const Admin = React.lazy(()=>import('./views/Admin/Table'));
const Register = React.lazy(()=>import('./views/Pages/Register'));
const Login = React.lazy(()=>import('./views/Pages/Login'));
const Image = React.lazy(()=>import('./views/Images/Image'));
const HomePage = React.lazy(()=>import('./views/LandingPage/HomePage'));


const App = () => {
      
         const useStyles = makeStyles(theme => ({
              root: {
                flexGrow: 1,
              },
              menuButton: {
                marginRight: theme.spacing(2),
              },
              title: {
                flexGrow: 1,
              },
            }));

            const theme = createMuiTheme({
              palette: {
                primary: {
                  light: '#fff350',  
                  main: '#ffc107',
                  dark: '#c79100',
                },
                secondary: {
                    light: '#ff8a50',
                    main: '#ff5722',
                    dark: '#c41c00',
                },
            },
});
            const classes = useStyles();



return(  
  <HashRouter>
    <React.Suspense fallback={Loading()}>
    <MuiThemeProvider theme={theme}> 
      <Switch>
        <Route 
          path="/"
          exact
          name="HomePage"
          render={props => <HomePage {...props} />}
        />
        <Route 
          path="/login"
          exact
          name="Login"
          render={props => <Login {...props} />}
        />
        <Route 
          path="/register"
          exact
          name="Register"
          render={props => <Register {...props} />}
        />
        <Route 
          path="/admin"
          exact
          name="Admin"
          render={props => <Admin {...props} />}
        />
        <Route 
          path="/upload"
          exact
          name="Upload Images"
          render={props => <Image {...props} />}
        />
        <Route  
          patch="/main"
          exact
          name="Main"
          render={props => <Main {...props} />}
        />
      </Switch>
    </MuiThemeProvider>   
    </React.Suspense>
  </HashRouter>
)}


export default App;
